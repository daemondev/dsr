# Deepin Screen Recorder

This is default screen recorder application for Deepin.

## Dependencies

In debian, use below command to install compile dependencies:


In debian, use below command to install running dependencies:
libc6 (>= 2.14), libgcc1 (>= 1:3.0), libgl1-mesa-glx | libgl1, libqt5core5a (>= 5.6.0~beta), libqt5dbus5 (>= 5.0.2), libqt5gui5 (>= 5.2.0), libqt5network5 (>= 5.0.2), libqt5widgets5 (>= 5.0.2), libqt5x11extras5 (>= 5.6.0), libstdc++6 (>= 4.1.1), libx11-6, libxcb-util0 (>= 0.3.8), libxcb1, libxext6, libxtst6

## Installation in any Debian based distro
Get latest version from "dtkwidget-dev" library. Try download from Ubuntu

* add "deb http://cz.archive.ubuntu.com/ubuntu eoan main universe" to /etc/apt/sources.list and run "sudo apt update" [https://packages.ubuntu.com/eoan/amd64/libdtkwidget-dev/download]
* run "sudo apt install libdtkwidget-dev(2.0.9.17)"
* in config file [~/.config/deepin/deepin-screen-recorder/config.conf] add param "record_audio=true" and optional "audio_system, audio_input and audio_codec" default are "alsa, pulse and pcm_s16le"
* download this repo and try next steps or.
* run all in a single command
```bash
sudo apt update -y && sudo apt install g++ git qttools5-dev-tools qt5-default libxcb-util0-dev libdtkwidget-dev libdtkwm-dev libproc-processtable-perl libprocps-dev libqt5x11extras5 libxtst6 libgl1 libqt5dbus5 libx11-6 libqt5dbus5 libxtst-dev -y && git clone https://gitlab.com/daemondev/dsr.git &&  cd dsr && mkdir build && cd build && qmake .. && make && sudo cp /usr/bin/deepin-screen-recorder{,.bkp} && sudo make install && printf "\n\nSUCCESS INSTALLATION [deepin-screen-recorder] with audio record support\nadd 'record_audio=true' to ~/.config/deepin/deepin-screen-record/config.conf\n\n"
```

## Troubleshooting
If have problem with video output(not save video), try add to ~/.config/deepin/deepin-screen-recorder/config.conf

```conf
[fileformat]
...
audio_input=default
...
```


* Optional set other config settings like this (lossless_recording for low compression and [mp4|mkv] framerate for better quality ):

```conf
lossless_recording=true
mkv_framerate=60
mp4_framerate=60
```

![](image/config.png)

## Installation

* mkdir build
* cd build
* qmake ..
* make
* ./deepin-screen-recorder

## Usage

1. Select area need to record
2. Then click "record" button to record
3. Click tray icon to stop record, and save file on desktop

Below is keymap list for deepin-screen-recorder:

| Function					      | Shortcut                                         |
|---------------------------------|--------------------------------------------------|
| Cancel                          | **ESC**                                          |
| Adjust position of select area  | **Up**, **Down**, **Left**, **Right**            |
| Adjust size of select area      | **Ctrl** + **Up**, **Down**, **Left**, **Right** |
| Stop record                     | Same keystroke that start deepin-screen-recorder |


## Config file
Configure file save at:
~/.config/deepin/deepin-screen-recorder/config.conf

You can change default format for save file.

## Getting help

Any usage issues can ask for help via

* [Gitter](https://gitter.im/orgs/linuxdeepin/rooms)
* [IRC channel](https://webchat.freenode.net/?channels=deepin)
* [Forum](https://bbs.deepin.org)
* [WiKi](http://wiki.deepin.org/)

## Getting involved

We encourage you to report issues and contribute changes

* [Contribution guide for users](http://wiki.deepin.org/index.php?title=Contribution_Guidelines_for_Users)
* [Contribution guide for developers](http://wiki.deepin.org/index.php?title=Contribution_Guidelines_for_Developers).

## License

Deepin Screen Recorder is licensed under [GPLv3](LICENSE).
